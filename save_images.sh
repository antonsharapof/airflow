#!/bin/bash

# Создаем директорию для сохранения образов
mkdir -p docker_images

# Получаем список всех образов
images=$(docker images --format "{{.Repository}}:{{.Tag}}")

# Сохраняем и архивируем каждый образ
for image in $images; do
    # Замена двоеточия на подчеркивание для имени файла
    filename=$(echo $image | tr '/:' '_' )
    echo $image
    # Сохранение образа
    #docker save -o "docker_images/${filename}.tar" "$image"

    # Архивирование
    #gzip "docker_images/${filename}.tar"
done

echo "Все образы сохранены и архивированы в папке docker_images"
