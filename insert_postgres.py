from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.postgres_hook import PostgresHook
from datetime import datetime, timedelta
import time


def insert_time_to_db():
    # Подключение к базе данных
    pg_hook = PostgresHook(postgres_conn_id='postgres_default')

    # Получение текущего времени
    current_time = datetime.now()

    # Выполнение SQL-запроса
    sql = "INSERT INTO time_records (recorded_time) VALUES (%s);"
    pg_hook.run(sql, parameters=(current_time,))


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 5, 17),
    'retries': 1,
    'retry_delay': timedelta(seconds=5),
}

with DAG('insert_time_to_db_dag',
         default_args=default_args,
         schedule_interval='*/10 * * * * *',  # Каждые 10 секунд
         catchup=False) as dag:
    insert_time_task = PythonOperator(
        task_id='insert_time_task',
        python_callable=insert_time_to_db
    )

insert_time_task
