from datetime import datetime, timedelta
from time import sleep
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import os

# Дефолтные аргументы для DAG
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2024, 5, 16),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(days=50),
}

# Создание DAG
dag = DAG(
    'simple-dimple',
    default_args=default_args,
    description='A simple test DAG',
    schedule_interval=timedelta(days=1),
)

# Первая задача: печатает текущее время
def print_time():
    print(f"Current time is: {datetime.now()}")

print_time_task = PythonOperator(
    task_id='print_time',
    python_callable=print_time,
    dag=dag,
)

# Вторая задача: создает файл
def create_file():
    sleep(30)
    with open('/tmp/test_file.txt', 'w') as f:
        f.write('Hello, World!')

create_file_task = PythonOperator(
    task_id='create_file',
    python_callable=create_file,
    dag=dag,
)

# Третья задача: проверяет, существует ли файл
def check_file():
    if os.path.exists('/tmp/test_file.txt'):
        print("File exists")
    else:
        print("File does not exist")

check_file_task = PythonOperator(
    task_id='check_file',
    python_callable=check_file,
    dag=dag,
)

# Определение порядка выполнения задач
print_time_task >> create_file_task >> check_file_task
