#!/bin/bash

# Переменные
REMOTE_REPO="your_harbor_repo"
HARBOR_URL="your_harbor_url"
HARBOR_PROJECT="your_project"
HARBOR_USERNAME="your_username"
HARBOR_PASSWORD="your_password"

# Вход в Harbor
echo $HARBOR_PASSWORD | docker login $HARBOR_URL -u $HARBOR_USERNAME --password-stdin

# Проверка, был ли вход успешным
if [ $? -ne 0 ]; then
  echo "Вход в Harbor не удалось."
  exit 1
fi

# Получение списка всех локальных образов
IMAGES=$(docker images --format "{{.Repository}}:{{.Tag}}" | grep -v "<none>")

# Обработка каждого образа
for IMAGE in $IMAGES; do
  # Тэгирование образа
  REMOTE_IMAGE="$HARBOR_URL/$HARBOR_PROJECT/$(echo $IMAGE | sed "s/.*\///")"
  docker tag $IMAGE $REMOTE_IMAGE

  # Проверка, был ли тэгирование успешным
  if [ $? -ne 0 ]; then
    echo "Тэгирование образа $IMAGE не удалось."
    continue
  fi

  # Пуш образа в удаленный репозиторий
  docker push $REMOTE_IMAGE

  # Проверка, был ли пуш успешным
  if [ $? -ne 0 ]; then
    echo "Пуш образа $REMOTE_IMAGE не удалось."
    continue
  fi

  echo "Образ $IMAGE успешно тэгирован и отправлен в репозиторий $REMOTE_IMAGE."
done

# Выход из Harbor
docker logout $HARBOR_URL